#!/bin/sh

set -e

export TIMESTAMP=$(date +%y.%m.%d.%H.%M)

export TAG_VERSION=${CI_PIPELINE_IID}

echo 'current build tag ${TAG_VERSION}'

docker login -u ${CI_DEPLOY_USER} -p ${CI_DEPLOY_PASSWORD} ${CI_REGISTRY}

chmod +x ${CI_PROJECT_DIR}/sample/scripts/**.sh

find ${CI_PROJECT_DIR}/sample/scripts/ -type f -iname '*.sh' | sort -n | xargs -I {} sh {}; 
