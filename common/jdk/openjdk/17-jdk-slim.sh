#!/bin/sh

set -e

echo " "
echo " "
echo 'build openjdk-17-jdk-slim start'

BASE_IMAGE=openjdk:17-jdk-slim

# 17-jdk-slim
docker build \
    --build-arg BASE_IMAGE=${BASE_IMAGE} \
    -t openjdk:${TAG_VERSION} \
    -f ${CI_PROJECT_DIR}/common/jdk/openjdk/base/Dockerfile . --no-cache
docker image tag openjdk:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/openjdk:17-jdk-slim-${TAG_VERSION}
docker image tag openjdk:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/openjdk:17-jdk-slim
docker push ${CI_REGISTRY}/opcal-project/containers/openjdk:17-jdk-slim-${TAG_VERSION}
docker push ${CI_REGISTRY}/opcal-project/containers/openjdk:17-jdk-slim

echo 'build openjdk-17-jdk-slim finished'
echo " "
echo " "