#!/bin/sh

set -e

echo " "
echo " "
echo 'build docker-jdk-11 start'

# 11-jdk-focal-docker
docker build \
    -t docker-jdk:${TAG_VERSION} \
    -f ${CI_PROJECT_DIR}/common/jdk/docker-jdk/11/Dockerfile . --no-cache
docker image tag docker-jdk:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/docker-jdk:11
docker push ${CI_REGISTRY}/opcal-project/containers/docker-jdk:11

echo 'build docker-jdk-11 finished'
echo " "
echo " "
