#!/bin/sh

set -e

echo " "
echo " "
echo 'build eclipse-temurin-11-jdk-alpine start'

BASE_IMAGE=eclipse-temurin:11-jdk-alpine
GOSU_VERSION=$(curl https://api.github.com/repos/tianon/gosu/releases/latest | grep tag_name | cut -d '"' -f 4)

# 11-jdk-alpine
docker build \
    --build-arg BASE_IMAGE=${BASE_IMAGE} \
    --build-arg GOSU_VERSION=${GOSU_VERSION} \
    -t eclipse-temurin:${TAG_VERSION} \
    -f ${CI_PROJECT_DIR}/common/jdk/eclipse-temurin/base/alpine/Dockerfile . --no-cache
docker image tag eclipse-temurin:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/eclipse-temurin:11-jdk-alpine-${TAG_VERSION}
docker image tag eclipse-temurin:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/eclipse-temurin:11-jdk-alpine
docker push ${CI_REGISTRY}/opcal-project/containers/eclipse-temurin:11-jdk-alpine-${TAG_VERSION}
docker push ${CI_REGISTRY}/opcal-project/containers/eclipse-temurin:11-jdk-alpine

echo 'build eclipse-temurin-11-jdk-alpine finished'
echo " "
echo " "