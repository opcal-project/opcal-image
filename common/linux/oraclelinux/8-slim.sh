#!/bin/sh

set -e

echo " "
echo " "
echo 'build oraclelinux:8-slim start'

GOSU_VERSION=$(curl https://api.github.com/repos/tianon/gosu/releases/latest | grep tag_name | cut -d '"' -f 4)

# oraclelinux:8-slim
docker build \
    --build-arg GOSU_VERSION=${GOSU_VERSION} \
    -t oraclelinux:${TAG_VERSION} \
    -f ${CI_PROJECT_DIR}/common/linux/oraclelinux/8-slim/Dockerfile . --no-cache
docker image tag oraclelinux:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/oraclelinux:8-slim-${TIMESTAMP}
docker image tag oraclelinux:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/oraclelinux:8-slim
docker push ${CI_REGISTRY}/opcal-project/containers/oraclelinux:8-slim-${TIMESTAMP}
docker push ${CI_REGISTRY}/opcal-project/containers/oraclelinux:8-slim

echo 'build oraclelinux:8-slim finished'
echo " "
echo " "