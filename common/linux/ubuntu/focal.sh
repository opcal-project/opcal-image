#!/bin/sh

set -e

echo " "
echo " "
echo 'build ubuntu:focal start'

BASE_IMAGE=ubuntu:focal

# ubuntu:focal
docker build \
    --build-arg BASE_IMAGE=${BASE_IMAGE} \
    -t ubuntu:${TAG_VERSION} \
    -f ${CI_PROJECT_DIR}/common/linux/ubuntu/base/Dockerfile . --no-cache
docker image tag ubuntu:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/ubuntu:focal-${TIMESTAMP}
docker image tag ubuntu:${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/ubuntu:focal
docker push ${CI_REGISTRY}/opcal-project/containers/ubuntu:focal-${TIMESTAMP}
docker push ${CI_REGISTRY}/opcal-project/containers/ubuntu:focal

echo 'build ubuntu:focal finished'
echo " "
echo " "