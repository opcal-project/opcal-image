#!/bin/sh

set -e

echo " "
echo " "
echo 'build linux start'

# alpine
find ${CI_PROJECT_DIR}/common/linux/alpine/ -type f -iname '*.sh' | sort -n | xargs -I {} sh {};

# oraclelinux
find ${CI_PROJECT_DIR}/common/linux/oraclelinux/ -type f -iname '*.sh' | sort -n | xargs -I {} sh {};

# ubuntu
find ${CI_PROJECT_DIR}/common/linux/ubuntu/ -type f -iname '*.sh' | sort -n | xargs -I {} sh {};

echo 'build linux finished'
echo " "
echo " "