#!/bin/sh

set -e

echo " "
echo " "
echo 'build opcal-jdk start'

# openjdk
find ${CI_PROJECT_DIR}/common/jdk/openjdk/ -type f -iname '*.sh' | sort -n | xargs -I {} sh {};

# eclipse-temurin
find ${CI_PROJECT_DIR}/common/jdk/eclipse-temurin/ -type f -iname '*.sh' | sort -n | xargs -I {} sh {};

# GraalVM
find ${CI_PROJECT_DIR}/common/jdk/graalvm/ -type f -iname '*.sh' | sort -n | xargs -I {} sh {};

# liberica-nik
find ${CI_PROJECT_DIR}/common/jdk/liberica-nik/ -type f -iname '*.sh' | sort -n | xargs -I {} sh {};

# docker-jdk
find ${CI_PROJECT_DIR}/common/jdk/docker-jdk/ -type f -iname '*.sh' | sort -n | xargs -I {} sh {};

echo 'build opcal-jdk finished'
echo " "
echo " "